package com.diobulk.filmujo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mathis on 3/25/2017.
 * Permet de, en fonction d'un string JSON, créer des objets JSON
 */

public class ParseJSON {
    public static final String JSON_ARRAY = "results";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DATE = "release_date";
    public static final String KEY_PAGE = "page"; //Non-implémenté (pour la pagination)
    public static String[] titles;
    public static String[] dates;
    public static JSONArray movies = null;

    private String json;

    public ParseJSON(String json){
        this.json = json;
    }

    protected boolean parseJSON(int nbResults) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(json);
            movies = jsonObject.getJSONArray(JSON_ARRAY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (movies.length() > nbResults) {
            resultSetter(nbResults, movies);
        } else {
            resultSetter(movies.length(), movies);
        }

        return movies.length() > 0;
    }

    /*
    nb = number of results
    movies = array of movies
     */
    private void resultSetter(int nb, JSONArray movies) {
        try {
            titles = new String[nb];
            dates = new String[nb];
            for (int i = 0; i < nb; i++) {
                JSONObject jo = movies.getJSONObject(i);
                titles[i] = jo.getString(KEY_TITLE);
                dates[i] = jo.getString(KEY_DATE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
