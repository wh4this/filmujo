package com.diobulk.filmujo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import static com.diobulk.filmujo.R.id.list_view;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static final String URL = "https://api.themoviedb.org/3/search/movie?api_key=b2e256b3188558bc6988f6b0f5a59a18&include_adult=false&query=";

    private String request;
    private EditText mEditText;
    private Button button;
    private RequestQueue mRequestQueue;
    private ListView listView;
    private ProgressBar pb;
    private String[] nbResultsStrings = {"All", "15 results", "10 results", "5 results", "Feeling lucky"};
    private Spinner spinner;
    private int nbResults = 20;
    private Intent i;
    String responseHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        pb = (ProgressBar) findViewById(R.id.progressBar);
        button = (Button) findViewById(R.id.SearchButton);
        mEditText = (EditText) findViewById(R.id.SearchEditText);
        listView = (ListView) findViewById(list_view);
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter_nbResults = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, nbResultsStrings);
        spinner.setAdapter(adapter_nbResults);
        request = "";
        spinner.setOnItemSelectedListener(this);
        pb.setVisibility(View.GONE);

        // Request Queue
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();

        Intent intent = getIntent();
        responseHome = intent.getStringExtra("response");
        pb.setVisibility(View.VISIBLE);
        showJSON(responseHome);
        pb.setVisibility(View.GONE);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendRequest();
            }
        });

        mEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            sendRequest();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    JSONObject movie = ParseJSON.movies.getJSONObject(position);
                    Intent i = new Intent(MainActivity.this, DetailsActivity.class);
                    i.putExtra("movie", movie.toString());
                    startActivity(i);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Movie JSON error", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void onNothingSelected(AdapterView<?> arg0) {
        nbResults = 20;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinner.setSelection(position);
        String spinnerState = (String) spinner.getSelectedItem();
        switch (spinnerState) {
            case "All":
                nbResults = 20;
                break;
            case "15 results":
                nbResults = 15;
                break;
            case "10 results":
                nbResults = 10;
                break;
            case "5 results":
                nbResults = 5;
                break;
            case "Feeling lucky":
                nbResults = 1;
                break;
        }
    }

    private void showJSON(String json) {
        ParseJSON pj = new ParseJSON(json);
        if (pj.parseJSON(nbResults)) {
            CustomList cl = new CustomList(this, ParseJSON.titles, ParseJSON.dates, nbResults);
            listView.setAdapter(cl);
            listView.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getApplicationContext(), "No results", Toast.LENGTH_SHORT).show();
            listView.setVisibility(View.GONE);
        }
    }

    private void sendRequest() {
        listView.setVisibility(View.GONE);
        request = mEditText.getText().toString();
        request = request.replaceAll("\\?", "");
        request = request.replaceAll("=", "");
        request = request.replaceAll("&", "and");
        request = request.replaceAll(" ", "+");

        if (!(request.isEmpty())) {
            // Remove the keyboard when the button is clicked
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            // Hide the progress bar
            pb.setVisibility(View.VISIBLE);

            // Send the request
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL + request,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(nbResults == 1) {
                                try {
                                    pb.setVisibility(View.GONE);
                                    ParseJSON pj = new ParseJSON(response);
                                    if(pj.parseJSON(nbResults)) {
                                        JSONObject movie = ParseJSON.movies.getJSONObject(0);
                                        Intent i = new Intent(MainActivity.this, DetailsActivity.class);
                                        i.putExtra("movie", movie.toString());
                                        startActivity(i);
                                    } else {
                                        Toast.makeText(getApplicationContext(), "No results", Toast.LENGTH_SHORT).show();
                                        listView.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Movie JSON error", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                showJSON(response);
                                pb.setVisibility(View.GONE);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Request error, check connectivity", Toast.LENGTH_SHORT).show();
                }
            });
            mRequestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "Enter something in the search field", Toast.LENGTH_SHORT).show();
        }
    }
}